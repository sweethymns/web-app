export default {
  home: {
    title: 'Home',
    hymnOfTheDay: 'Hymn of the day'
  },
  hymns: {
    title: 'Hymns'
  },
  hymnals: {
    title: 'Hymnals'
  }
}
