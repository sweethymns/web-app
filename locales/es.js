export default {
  home: {
    title: 'Inicio',
    hymnOfTheDay: 'Himno del Día'
  },
  hymns: {
    title: 'Himnos'
  },
  hymnals: {
    title: 'Himnarios'
  }
}
